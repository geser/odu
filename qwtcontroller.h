#ifndef QWTCONTROLLER_H
#define QWTCONTROLLER_H

#include <QObject>
#include <functional>


class QwtPlot;

class QwtController : public QObject
{
    Q_OBJECT
public:
    explicit QwtController(QwtPlot* uiplot, uint pointsCount, QObject *parent = 0);
    void SetQwtPlot(QwtPlot* uiplot);
    void AddFunction(std::function<double (double)> newFunc, double leftBorder, double rightBorder, int width=1);
    void Clear();

signals:

public slots:

private:
    QwtPlot* plot;
    uint accuracy;
    uint lastColor;
    Qt::GlobalColor colors[3];

};

#endif // QWTCONTROLLER_H
