#include "qwtcontroller.h"
#include "qwt_plot.h"
#include "qwt_plot_curve.h"

QwtController::QwtController(QwtPlot *uiplot, uint pointsCount, QObject *parent) :
    QObject(parent),
    plot(uiplot),
    accuracy(pointsCount),
    lastColor(2)
{
    colors[0] = Qt::red;
    colors[1] = Qt::blue;
    colors[2] = Qt::darkGreen;
}

void QwtController::AddFunction(std::function<double (double)> newFunc, double leftBorder, double rightBorder, int width)
{
    double* x = new double[accuracy];
    double* y = new double[accuracy];

    for (int i=0; i<accuracy; i++)
    {
        x[i] = (rightBorder-leftBorder)*i/accuracy + leftBorder;
        y[i] = newFunc(x[i]);
    }

    QPen pen(colors[(lastColor)%3]);
    pen.setWidth(width);
    QwtPlotCurve* newCurve;
    newCurve = new QwtPlotCurve("Target function");
    newCurve->setPen(pen);
    newCurve->attach(plot);

    newCurve->setData(x, y, accuracy);

    plot->replot();

    delete[] x;
    delete[] y;
}

void QwtController::Clear()
{
    plot->detachItems(QwtPlotCurve::Rtti_PlotCurve);
    lastColor = 2;

    plot->replot();
}

void QwtController::SetQwtPlot(QwtPlot *uiplot)
{
    plot = uiplot;
}
