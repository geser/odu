#ifndef ODUSOLVER_H
#define ODUSOLVER_H

#include "utils.h"
#include "QApplication"
#include <QDebug>
#include "math.h"



template <typename FuncResult>
class OduSolver
{
public:
    OduSolver()
    {
    }

    //typedef QVector<double>::iterator FuncIterator;
    //typedef QVector<double>::iterator ArgIterator;

    typedef uint FuncIterator;
    typedef uint ArgIterator;

    typedef std::function<void (FuncIterator uBegin, FuncIterator uEnd,
				ArgIterator xBegin, ArgIterator xEnd)> SolverCallBack;

    void init(std::function<FuncResult (double)> rightPart, SolverCallBack curResultsCallback)
    {
	f = rightPart;
	bStoped = false;
	callBack = curResultsCallback;
    }

    void StopSolving()
    {
	bStoped = true;
    }

    void Solve(QVector<FuncResult>& u, QVector<double>& x, QVector<double>& h, double x0, FuncResult u0, double startStep, double eps, uint maxIter, bool bDoubleHalf)
    {
	x.clear();
	u.clear();
	h.clear();
	locEstimate.clear();

	x.push_back(x0);
	u.push_back(u0);
	h.push_back(startStep);
	locEstimate.push_back(0.0);

	lastFuncIter = 0;
	lastArgIter = 0;

	StepResult lastResult;
	lastResult.x = x0;
	lastResult.u = u0;
	lastResult.h = startStep;
	lastResult.locEstimate = 0.0;

	uint counter = 0;

	while(!bStoped)
	{
	    lastResult = step(lastResult.x, lastResult.u, lastResult.h, eps, bDoubleHalf);

	    x.push_back(lastResult.x);
	    u.push_back(lastResult.u);
	    h.push_back(lastResult.h);
	    locEstimate.push_back(lastResult.locEstimate);

	    counter++;
	    if(counter%10 == 9)
	    {
		/*callBack(lastFuncIter, u.size(), lastArgIter, x.size());
		lastFuncIter = u.size() - 1;
		lastArgIter = u.size() - 1;*/
		QApplication::processEvents();
	    }

	    if(counter>maxIter)
	    {
		bStoped = true;
	    }

	}

	callBack(lastFuncIter, u.size(), lastArgIter, x.size());
	lastFuncIter = u.size() - 1;
	lastArgIter = u.size() - 1;



    }

    QVector<double>& GetLocEstimate()
    {
	return locEstimate;
    }



private:
    FuncResult stepFunc(double x, FuncResult v, double h)
    {
	return v + (h/2.0)*(f(v) + f(v + h*(f(v))));
    }

    struct StepResult
    {
	double x;
	FuncResult u;
	double h;
	double locEstimate;
    };

    StepResult step(double x0, FuncResult u0, double startStep, double eps, bool bDoubleHalf)
    {
	double curX = x0;
	double curH = startStep;
	double curV = u0;
	double curV1;
	double curV12;
	double curV2;
	double S;
	double p = 2; // ������� ������
	double lowSBorder = eps/(pow(2.0, p+1.0));
	StepResult result;

	curV1 = stepFunc(curX, curV, curH);
	curV12 = stepFunc(curX, curV, curH*0.5);
	curV2 = stepFunc(curX + curH*0.5, curV12, curH*0.5);

	S = (curV2 - curV1)*pow(2.0, p)/double(pow(2.0, p) - 1.0);
	result.locEstimate = S;

	if (!bDoubleHalf) // �� ���������� ������� ���� � ���������� �����
	{
	    result.x = curX + curH;
	    result.u = curV1;
	    result.h = curH;
	}
	else if (fabs(S)<lowSBorder)
	{
	    result.x = curX + curH;
	    result.u = curV1;
	    result.h = curH*2.0;
	}
	else if (fabs(S)<eps)
	{
	    result.x = curX + curH;
	    result.u = curV1;
	    result.h = curH;
	}
	else
	{
	    result = step(curX, curV, curH*0.5, eps, bDoubleHalf);
	}

	return result;
    }

    std::function<FuncResult (double)> f;
    SolverCallBack callBack;

    FuncIterator lastFuncIter;
    ArgIterator lastArgIter;

    QVector<FuncResult> resilts;

    QVector<double> locEstimate;

    bool bStoped;
};

#endif // ODUSOLVER_H
