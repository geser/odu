#include "functions.h"
#include <QDebug>

double rightPartFunc(double a1, double a2, double arg)
{
    return -(a1*arg + a2*arg*arg*arg);
}

DoubleFunc analiticRes(double a1, double a2, double m, double u0, double x0)
{
    double c = u0*exp(a1*x0/m);

    return std::bind(&_analiticRes, a1, a2, m, c, std::placeholders::_1);
}

double _analiticRes(double a1, double a2, double m, double c, double arg)
{
    return c*exp(-a1*arg/m);
}
