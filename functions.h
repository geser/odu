#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "math.h"
#include "utils.h"

double rightPartFunc(double a1, double a2, double arg);

DoubleFunc analiticRes(double a1, double a2, double m, double u0, double x0);

double _analiticRes(double a1, double a2, double m, double c, double arg);


#endif // FUNCTIONS_H
