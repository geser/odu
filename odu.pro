#-------------------------------------------------
#
# Project created by QtCreator 2012-03-03T18:36:26
#
#-------------------------------------------------

QT       += core gui

TARGET = odu
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    OduModel.cpp \
    utils.cpp \
    qwtcontroller.cpp \
    DoubleTableModel.cpp \
    functions.cpp

HEADERS  += mainwindow.h \
    OduModel.h \
    utils.h \
    qwtcontroller.h \
    DoubleTableModel.h \
    OduSolver.h \
    functions.h

FORMS    += mainwindow.ui

INCLUDEPATH += qwt/

release: LIBS += d:/QtSdk/libqwt5.a
debug: LIBS += d:/QtSdk/libqwtd5.a

QMAKE_CXXFLAGS += -std=c++0x











