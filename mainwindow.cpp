#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"
#include "functions.h"
#include "math.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qwtController = new QwtController(ui->qwtPlot, 200, this);

    connect(ui->button_Start, SIGNAL(clicked()), SLOT(startBtnPressed()));
    connect(ui->button_Stop, SIGNAL(clicked()), SLOT(stopBthPressed()));
    connect(ui->button_Reset, SIGNAL(clicked()), SLOT(resetBthPressed()));

    ui->tableView->verticalHeader()->setVisible(false);
    ui->tableView->horizontalHeader()->setVisible(false);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startBtnPressed()
{
    double a1 = ui->lineEdit_a1->text().toDouble();
    double a2 = ui->lineEdit_a2->text().toDouble();
    double x0 = ui->lineEdit_x0->text().toDouble();
    double u0 = ui->lineEdit_U0->text().toDouble();
    double h = ui->lineEdit_h->text().toDouble();
    double eps = ui->lineEdit_eps->text().toDouble();
    double m = 1.0;
    uint max = ui->lineEdit_MAX->text().toUInt();
    bool bDoubleHalf = ui->useDoubleHalf->isChecked();

    double c = -x0 - m*0.5*log(a1 + a2*u0*u0) + m*log(u0)/a1;
    qDebug() << "c = " << c;

    //u = std::bind(&analiticRes, a1, a2, m, abs(c), std::placeholders::_1);

    emit start(x0, u0, h, a1, a2, eps, max, bDoubleHalf);
}

void MainWindow::stopBthPressed()
{
    emit stop();
}

void MainWindow::updateGraph(DoubleFunc newFunc, double left, double right)
{
    double a = (left<right)? left : right;
    double b = (left<right)? right : left;
    qwtController->AddFunction(newFunc, a, b);
//    qwtController->AddFunction(u, left, right);
}

void MainWindow::setTableModel(QAbstractItemModel *model)
{
    ui->tableView->setModel(model);
}

void MainWindow::resetBthPressed()
{
    qwtController->Clear();
    emit reset();

}
