#ifndef UTILS_H
#define UTILS_H

#include <functional>
#include <vector>
#include <algorithm>
#include <QVector>

class Utils
{
public:
    Utils();
};



typedef  std::function<double (double)> DoubleFunc;
typedef QVector<double>::iterator DoubleIterator;

double Greater(double first, double second);


double LinearApproximatePoint(const QVector<double>& x, const QVector<double>& y, double arg);
double LinearApproximatePointIter(DoubleIterator xBegin, DoubleIterator xEnd, DoubleIterator yBegin, DoubleIterator yEnd, double arg);

DoubleFunc LinearInterpolation(const QVector<double>& x, const QVector<double>& y);
DoubleFunc LinearInterpolation(DoubleIterator xBegin, DoubleIterator xEnd, DoubleIterator yBegin, DoubleIterator yEnd);


double AverageRectIntegralIter(DoubleFunc func, double left, double right);


#endif // UTILS_H
