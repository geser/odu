#ifndef DOUBLETABLEMODEL_H
#define DOUBLETABLEMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QVector>
#include <memory>

typedef std::shared_ptr<QVector<double> > DoubleRow;

class DoubleTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit DoubleTableModel(uint columnCount, QObject *parent = 0);

    void addRow(DoubleRow rowData);
    void clear();

    QVariant data(const QModelIndex &index, int role) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;


    void emitChadges();

signals:

public slots:

private:
    QVector<DoubleRow> table;
    QVector<QVariant> headers;
    int m_columnCount;

};

#endif // DOUBLETABLEMODEL_H
