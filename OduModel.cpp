#include "OduModel.h"
#include "functions.h"
#include <memory>

const uint ROW_COUNT = 5;

OduModel::OduModel(QObject *parent) :
    QObject(parent),
    tableModel(new DoubleTableModel(ROW_COUNT, this)),
    x(),
    u1(),
    h(),
    solver(new OduSolver<double>)

{
}

QAbstractItemModel * OduModel::model()
{
    return tableModel;
}

void OduModel::start(double x0, double u0, double startStep, double a1, double a2, double eps, uint maxIter, bool bDoubleHalf)
{
    analitic = analiticRes(a1, a2, 1.0, u0, x0);
    solver->init(std::bind(&rightPartFunc, a1, a2, std::placeholders::_1), std::bind(&OduModel::updateData, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    solver->Solve(u1, x, h, x0, u0, startStep, eps, maxIter, bDoubleHalf);
}

void OduModel::stop()
{
    solver->StopSolving();
}

void OduModel::updateData(OduSolver<double>::FuncIterator uBegin, OduSolver<double>::FuncIterator uEnd, OduSolver<double>::ArgIterator xBegin, OduSolver<double>::ArgIterator xEnd)
{
    if(x.front() < x.back())
    {
	emit newData(LinearInterpolation(x.begin() + xBegin, x.begin() + xEnd, u1.begin() + uBegin, u1.begin() + uEnd), x[xBegin], x[xEnd-1]);
    }
    else
    {
	std::reverse(x.begin() + xBegin, x.begin() + xEnd);
	std::reverse(u1.begin() + uBegin, u1.begin() + uEnd);
	emit newData(LinearInterpolation(x.begin() + xBegin, x.begin() + xEnd, u1.begin() + uBegin, u1.begin() + uEnd), x[xBegin], x[xEnd-1]);
    }

    DoubleRow curRow (new Vector(ROW_COUNT));
    Vector& s = solver->GetLocEstimate();

    for(uint i = xBegin; i!=xEnd; i++)
    {
	curRow = DoubleRow(new Vector(ROW_COUNT));
	(*curRow)[0] = x.at(i);
	(*curRow)[1] = u1.at(i);
	(*curRow)[2] = h.at(i);
	(*curRow)[3] = s.at(i);
	(*curRow)[4] = analitic(x.at(i)) - u1.at(i);
	tableModel->addRow(curRow);
	qDebug() << (*curRow)[0];
    }

    tableModel->emitChadges();
}

void OduModel::reset()
{
    tableModel->clear();
}
