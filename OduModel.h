#ifndef ODUMODEL_H
#define ODUMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <QHash>
#include "utils.h"
#include "OduSolver.h"
#include "DoubleTableModel.h"

typedef QVector<double> Vector;

class OduModel : public QObject
{
    Q_OBJECT
public:
    explicit OduModel(QObject *parent = 0);

    QAbstractItemModel* model();

    void updateData(OduSolver<double>::FuncIterator uBegin, OduSolver<double>::FuncIterator uEnd,
		    OduSolver<double>::ArgIterator xBegin, OduSolver<double>::ArgIterator xEnd);

signals:
    void newData(DoubleFunc, double, double);

public slots:
    void start(double x0, double u0, double startStep, double a1, double a2, double eps, uint maxIter, bool bDoubleHalf);
    void stop();
    void reset();

private:
    DoubleTableModel* tableModel;

    Vector x;
    Vector u1;
    Vector h;

    DoubleFunc analitic;

    OduSolver<double>* solver;


};

#endif // ODUMODEL_H
