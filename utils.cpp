#include "utils.h"

Utils::Utils()
{
}


#ifndef FUNCTOOLS_H
#define FUNCTOOLS_H

#include <functional>
#include <vector>
#include <algorithm>

typedef  std::function<double (double)> DoubleFunc;

double Greater(double first, double second)
{
        return first>second;
}

double LinearApproximatePoint(const QVector<double>& x, const QVector<double>& y, double arg)
{
    double res;

    auto rightBorder = std::find_if(x.begin(), x.end(), std::bind(&Greater, std::placeholders::_1, arg)); // TODO: ��������������, ������� �������� �������

    if(rightBorder == x.begin())
    {
            res =  y.front();
    }

    if (rightBorder == x.end())
    {
            res = y.back();
    }

    auto leftBorder = rightBorder - 1;

    double x1 = *leftBorder;
    double x2 = *rightBorder;


    double y1 = *((leftBorder - x.begin()) + y.begin());
    double y2 = *((rightBorder - x.begin()) + y.begin());

    double weight = (x2 - arg)/(x2-x1);


    res = weight*y1 + (1.0 - weight)*y2;

    return res;
}

DoubleFunc LinearInterpolation(const QVector<double>& x, const QVector<double>& y)
{
    return std::bind(&LinearApproximatePoint, x, y, std::placeholders::_1);
}

double AverageRectIntegralIter(DoubleFunc func, double left, double right)
{
    return (func(left)+func(right))/(2.0*(right-left));
}

double LinearApproximatePointIter(DoubleIterator xBegin, DoubleIterator xEnd, DoubleIterator yBegin, DoubleIterator yEnd, double arg)
{
    double res;

    auto rightBorder = std::find_if(xBegin, xEnd, std::bind(&Greater, std::placeholders::_1, arg)); // TODO: ��������������, ������� �������� �������

    if(rightBorder == xBegin)
    {
	    res =  *yBegin;
    }

    if (rightBorder == xEnd)
    {
	res = *(yEnd - 1);
    }

    auto leftBorder = rightBorder - 1;

    double x1 = *leftBorder;
    double x2 = *rightBorder;


    double y1 = *((leftBorder - xBegin) + yBegin);
    double y2 = *((rightBorder - xBegin) + yBegin);

    double weight = (x2 - arg)/(x2-x1);


    res = weight*y1 + (1.0 - weight)*y2;

    return res;
}

DoubleFunc LinearInterpolation(DoubleIterator xBegin, DoubleIterator xEnd, DoubleIterator yBegin, DoubleIterator yEnd)
{
    return std::bind(&LinearApproximatePointIter, xBegin, xEnd, yBegin, yEnd, std::placeholders::_1);
}

#endif // FUNCTOOLS_H
