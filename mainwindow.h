#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qwtcontroller.h"
#include "utils.h"

#include <QAbstractItemModel>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setTableModel(QAbstractItemModel* model);

signals:
    void start(double x0, double u0, double startStep, double a1, double a2, double eps, uint max, bool bDoubleHalf);
    void stop();
    void reset();

public slots:
    void updateGraph(DoubleFunc newFunc, double left, double right);

private slots:
    void startBtnPressed();
    void stopBthPressed();
    void resetBthPressed();

private:
    Ui::MainWindow *ui;
    QwtController* qwtController;
    DoubleFunc u;
};

#endif // MAINWINDOW_H
