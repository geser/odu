#include <QtGui/QApplication>
#include "mainwindow.h"
#include "OduModel.h"
#include <memory>
#include <list>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    OduModel model;

    w.setTableModel(model.model());


    a.connect(&w, SIGNAL(start(double,double,double,double,double,double,uint,bool)),
	      &model, SLOT(start(double,double,double,double,double,double,uint,bool)));
    a.connect(&w, SIGNAL(stop()), &model, SLOT(stop()));
    a.connect(&w, SIGNAL(reset()), &model, SLOT(reset()));

    a.connect(&model, SIGNAL(newData(DoubleFunc,double,double)), &w, SLOT(updateGraph(DoubleFunc,double,double)));

    return a.exec();
}
