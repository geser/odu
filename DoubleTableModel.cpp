#include "DoubleTableModel.h"
#include "QDebug"

DoubleTableModel::DoubleTableModel(uint columnCount, QObject *parent) :
    QAbstractTableModel(parent),
    headers(columnCount, "123"),
    m_columnCount(columnCount)
{
}

QVariant DoubleTableModel::data(const QModelIndex &index, int role) const
{
    QVariant result;

    if(!index.isValid()) return QVariant(); // прверка валидности индекса

    int row = index.row();
    int column = index.column();

    if((row < table.size()) && (column < table[row]->size()))
    {
	result = (role == Qt::DisplayRole || role == Qt::EditRole) ? table[row]->at(column) : QVariant();
	//qDebug() << QString("(%1, %2) = %3").arg(row).arg(column).arg(result.toDouble());
	return result;
    }
    else
    {
	qDebug() << QString("index invalid");
	return QVariant();
    }


}

bool DoubleTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    /*if(!index.isValid()) return false; // прверка валидности индекса

    int row = index.row();
    int column = index.column();

    if((column < table.size()) && (row < table[column].size()) && (role == Qt::EditRole))
    {
	table[column][row] = value.toDouble();
	emit dataChanged(index, index);
	return true;
    }
    else
    {
	return false;
    }*/

    qDebug() << "Writing to DoubleTableModel is prohibit";
    return false;

}

int DoubleTableModel::rowCount(const QModelIndex &parent) const
{
    return table.size();
}

int DoubleTableModel::columnCount(const QModelIndex &parent) const
{
    return m_columnCount;
}

Qt::ItemFlags DoubleTableModel::flags(const QModelIndex &index) const
{
    return QAbstractTableModel::flags(index);
}

void DoubleTableModel::emitChadges()
{
    if(!table.empty())
    {
	/*for(auto iter = table.begin(); iter!= table.end(); iter++)
	{
	    qDebug() << (*iter)->at(0);
	}*/

	emit reset();
    }
}

void DoubleTableModel::addRow(DoubleRow rowData)
{
    if (rowData->size() == m_columnCount)
    {
	table.push_back(rowData);
    }
    else
    {
	qDebug() << "DoubleTableModel::addRow row size is unnormal";
    }
}

void DoubleTableModel::clear()
{
    table.clear();
    emit reset();
}



